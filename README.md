# macOS Setup Steps
This is the setup process that I go through to set up a Macbook Pro from Partnerize.

There are a few sections here:
- [System Preferences](#system-preferences)
- [Uninstall Bloatware](#uninstall-bloatware)
- [Install Applications](#install-applications)
- [Configure Applications](#configure-applications)

## System Preferences
- Security & Privacy > General > "Change Password..."
- Displays
    - _(optional)_ Resolution = Scaled (`More Space`)
    - "Night Shift..." > Schedule = `Sunset to Sunrise`
- Trackpad
    - Point & Click
        - Tracking Speed = `Fastest` (or one lower)
        - Click = `Light`
    - Scroll & Zoom
        - "Scroll direction: Natural" = DISABLE
    - More Gestures
        - Swipe between full-screen apps = `four fingers`
        - Mission Control = `four fingers`
- Mouse
    - Point & Click
        - Secondary click = `right side` + ENABLE
        - "Smart zoom" = ENABLE
        - "Tracking speed" = `Fast`
    - More Gestures
        - Swipe between pages = ENABLE
- Keyboard
    - Keyboard
        - Key Repeat = `Fast`
        - Delay Until Repeat = `Short`
        - Turn keyboard backlight off after `10 secs` of inactivity = 10 secs + ENABLE
        - Press [globe] to ... = `Do Nothing`
        - Use F1, F2, etc. keys as standard function keys = ENABLE
    - Text
        - Correct spelling automatically = DISABLE
        - Capitalize words autoomatically = DISABLE
        - Add period with double-space = DISABLE
        - Use smart quotes and dashes = DISABLE
    - Shortcuts
        - "Use keyboard navigation to move focus between controls" (at bottom) = ENABLE
        - Launchpad & Dock
            - "Turn Dock Hiding On/Off" = DISABLE
        - Mission Control
            - "Turn Do Not Disturb On/Off" = DISABLE
                - _Note: it's unassigned by default anyway_
        - Accessibility
            - Zoom = ENABLE
- Accessibility
    - Zoom
        - "Use keyboard shortcuts to zoom" = ENABLE
        - "Use scroll gesture with modifier keys to zoom" = ENABLE
    - Pointer Control > Mouse & Trackpad > "Trackpad Options..." > "Enable dragging" = `three finger drag` + ENABLE
- Mission Control
    - "Automatically rearrange Spaces based on most recent use" = DISABLE
    - "Group windows by application" = ENABLE
- General
    - Appearance = Dark
    - "Click in the scroll bar to" = "Jump to the spot that's clicked"
    - Recent Items = None
    - "Allow Handoff between this Mac and your iCloud devices" = DISABLE
- Dock & Menu Bar
    - Dock & Menu Bar
        - "Position on screen" = "Left"
        - "Minimize windows into application icon" = ENABLE
    - Other Modules > Battery
        - "Show Percentage" = ENABLE
    - Menu Bar Only > Clock
        - "Show the day of the week" = ENABLE
        - "Show data" = ENABLE
        - "Use a 24-hour clock" = ENABLE
        - "Flash the time separators" = ENABLE
        - "Display the time with seconds" = ENABLE


## Uninstall Bloatware
For some reason, PZ machines come with bloatware installed.

1. Open "AppCleaner" (which may be considered bloatware, itself)
1. Drag-and-drop the following programs from the Applications folder to "AppCleaner":
    - Editors:
        - Atom
        - BBEdit
        - Sublime Text (outdated, needs reinstallation)
        - MySQL Workbench (outdated, needs reinstallation)
        - JetBrains Toolbox (outdated, needs reinstallation)
    - API tools:
        - Postman
        - Insomnia
    - Source Control GUIs:
        - GitKraken
        - SourceTree
    - Non-dev apps:
        - Commander One (requires a license to use; one is not provided)
        - Evernote
        - KeePassXC
        - Google Hangouts "Chrome App" (UNMAINTAINED)
        - SilentKnight
        - Skype
        - Spectacle (UNMAINTAINED)
        - Spotify

## Install Applications
- Authy Desktop - https://authy.com/download
- iTerm2 - https://iterm2.com/downloads.html
- VS Code - https://code.visualstudio.com/Download
- Sublime Text - https://www.sublimetext.com/download (license recommended)
- Sublime Merge - https://www.sublimemerge.com/ (license recommended)
- BetterTouchTool - https://folivora.ai/downloads (license required)
- KeyboardCleanTool - https://folivora.ai/downloads
- TopNotch - https://topnotch.app/ _(optional: to hide hardware notch without BetterTouchTool)_
- Numi (a wonderful calculator) - https://numi.app/
- Kap (screen recorder, open source) - https://getkap.co/
- KeyCastr (display key presses) - https://github.com/keycastr/keycastr/releases
- Plexamp - https://plexamp.com/#downloads (requires Plex)
- TickTick - https://ticktick.com/about/download
- Spark (email) - App Store > profile > Mac Apps > "Get"
- LastPass - App Store > profile > Mac Apps > "Get"
- Color Picker - App Store > profile > Mac Apps > "Get"
- AnyConnect (for Pepperjam) - Go to `vpn1.pepperjam.com` in a browser > enter PJ SSO credentials
- Homebrew - https://brew.sh/
    - `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- JetBrains Toolbox - https://www.jetbrains.com/toolbox-app/
    - Launch it, then "sign in" with your `@partnerize.com` email address and install the desired licensed software.
- MySQL Workbench - https://dev.mysql.com/downloads/workbench/
    - Click "Download" > press "No thanks, just start my download." > Install it
- Docker Desktop - https://www.docker.com/products/docker-desktop
    - **NOTE: It may be "disabled" by default.  If so, reach out to TechOps for support to enable it by following these steps:**
        1. Ask TechOps to exclude the device (provide Serial Number) from the rule blocking Docker Desktop access.
        1. Run `sudo jamf policy` once they've made the change.  This will "force" an ad-hoc recheck with the control server immediately instead of whatever schedule it would normally use.
        1. Restart.
        1. Launch Docker Desktop.  (_Note: I had to restart and launch it one more time before it actually worked fully._)


## Configure Applications
- Google Drive
    - Sign In > Hit `Allow` on all prompts
- Screenshots
    1. Open `screenshot.app` (ex: via Spotlight or `Command + Shift + 5` by default)
    1. Press "Options"
        - "Show floating thumbnail" = DISABLE
        - "Save To" > "Other Location..." = `~/Pictures/screenshots` (create if doesn't exist)
- Disable Apple Music launching from headphone jack activity
    - `launchctl unload -w /System/Library/LaunchAgents/com.apple.rcd.plist`
        - _Context: By default, using the headphone jack will frequently cause Apple Music to open.  This disables it._
- Zoom
    - Sign In (SSO) > `partnerize.zoom.us` > use email prefix (ex: `robert.robinson`)
    - Preferences
        - General
            - "Always show meeting controls" = ENABLE
            - "Show my meeting duration" = ENABLE
        - Video
            - "Touch up my appearance" = ENABLE + lower the slider
            - "Adjust for low light" = ENABLE + `Auto`
            - "Always display participant name on their videos" = ENABLE
            - "Stop my video when joining a meeting" = ENABLE
            - "Enable stop incoming video feature" = ENABLE
        - Audio
            - "Automatically join computer audio when joining a meeting" = ENABLE
            - "Mute my mic when joining a meeting" = ENABLE
        - Share Screen
            - "Window size when screen sharing" = `Maintain current size`
            - "When I share directly to a Zoom Room" = `Show all sharing options`
        - Phone
            - "Default for calls" = `zoom.us`
        - Chat
            - "Show Code Snippet button" = ENABLE
        - Background & Filters
            - "Virtual Backgrounds" = `Blur`        
- iTerm2
    1. (on old machine) iTerm2 > Preferences > General > Preferences
        - "Load preferences from a custom folder or URL" = ENABLE
        - Specify a filepath
        - Press "Save Now"
    1. Copy file (probably `com.googlecode.iterm2.plist`) to new machine
    1. (on new machine) iTerm2 > Preferences > General > Preferences
        - ...same thing...
        - "Save changes" = `Manually` (to avoid risk of overwriting the backup)
        - Specify the filepath
        - Quit iTerm2 (just for good measure)
        - Launch iTerm2 again
        - Confirm that settings were restored
        - "Save changes" = `Automatically`
- Finder = Launch > Preferences:
    - General > "Show these items on the desktop":
        - UNCHECK: [everything] <-- _note: apparently cannot do this_
    - Sidebar:
        - CHECK:
            - "$USER's MacBook Pro"
            - "$USER"
        - UNCHECK:
            - iCloud Drive
            - AirDrop
            - Cloud Storage
            - Bonjour Computers
            - Recent Tags
    - Advanced:
        - "Show all filename extensions"
